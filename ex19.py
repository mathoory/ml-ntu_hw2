
import random
import math
import numpy as np


def generate_sorted_dataset(n, tau):
    points = []
    for i in range(n):
        points.append(random.uniform(-0.5, 0.5))
    points = sorted(points)
    signs = [math.copysign(1, p) for p in points]

    if tau:
        for i in range(len(signs)):
            if random.random() < tau:
                signs[i] = -signs[i]

    return (points, signs)


def calc_ein(dataset, results, s, theta):
    errs = 0
    for i in range(len(dataset)):
        if s*math.copysign(1, dataset[i] - theta) != results[i]:
            errs += 1
    return errs/len(dataset)


def calc_eout(theta, tau):
    return min(math.fabs(theta), 0.5)*(1-tau)+tau


def pairwise_avg(arr):
    result = []
    for i in range(len(arr)-1):
        avg = (arr[i] + arr[i+1]) / 2.0
        result.append(avg)
    return result


def train_decision_stump_model(dataset, results):
    min_ein = 1
    best_theta = float('-inf')
    best_s = 0

    optional_theta = [float('-inf')]
    optional_theta += (pairwise_avg(dataset))

    for theta in optional_theta:
        for s in [-1, 1]:
            curr_ein = calc_ein(dataset, results, s, theta)

            if (curr_ein < min_ein or (curr_ein == min_ein and theta*s < best_theta*best_s)):
                min_ein = curr_ein
                best_theta = theta
                best_s = s

    return (best_s, best_theta, min_ein)


def read_columns_from_file(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()

    # Split the lines into columns
    columns = [line.split() for line in lines]

    # Convert the columns to arrays of numbers
    arrays = [list(map(float, col)) for col in zip(*columns)]

    return arrays


if __name__ == "__main__":

    N = 10

    train = read_columns_from_file("hw2_train.dat")
    x_train = train[0:N]
    y_train = train[N]

    test = read_columns_from_file("hw2_test.dat")
    x_test = test[0:N]
    y_test = test[N]

    s = [None for i in range(N-1)]
    theta = [None for i in range(N-1)]
    min_ein = [None for i in range(N-1)]

    # Train
    for i in range(N-1):
        s[i], theta[i], min_ein[i] = train_decision_stump_model(
            x_train[i], y_train)

    best_of_the_best = min_ein.index(min(min_ein))
    worst_of_the_best = min_ein.index(max(min_ein))

    # Test
    y_predict_bob = [s[best_of_the_best] *
                 math.copysign(1, x - theta[best_of_the_best]) for x in x_test[best_of_the_best]]
    y_predict_wob = [s[worst_of_the_best] *
                 math.copysign(1, x - theta[worst_of_the_best]) for x in x_test[worst_of_the_best]]

    # Calc err_out
    err_count_bob = 0
    for i in range(len(y_predict_bob)):
        if y_predict_bob[i] != y_test[i]:
            err_count_bob += 1
    e_out_bob = err_count_bob/len(y_test)
    
    err_count_wob = 0
    for i in range(len(y_predict_wob)):
        if y_predict_wob[i] != y_test[i]:
            err_count_wob += 1
    e_out_wob = err_count_wob/len(y_test)

    print(f"deltaEin: {min_ein[worst_of_the_best]-min_ein[best_of_the_best]}, deltaEout: {e_out_wob-e_out_bob}")