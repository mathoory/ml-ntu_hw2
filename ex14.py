
import random
import math
import statistics


def generate_sorted_dataset(n, tau):
    points = []
    for i in range(n):
        points.append(random.uniform(-0.5, 0.5))
    points = sorted(points)
    signs = [math.copysign(1,p) for p in points]
    
    if tau:
        for i in range(len(signs)):
            if random.random() < tau:
                signs[i] = -signs[i]

    return (points, signs)


def calc_ein (dataset, results, s, theta):
    errs = 0
    for i in range(len(dataset)):
        if s*math.copysign(1, dataset[i] - theta) != results[i]:
            errs += 1
    return errs/len(dataset)


def calc_eout(theta, tau):
    return min(math.fabs(theta), 0.5)*(1-tau)+tau


def pairwise_avg(arr):
    result = []
    for i in range(len(arr)-1):
        avg = (arr[i] + arr[i+1]) / 2.0
        result.append(avg)
    return result


def train_decision_stump_model(dataset, results):
    min_ein = 1
    best_theta = float('-inf')
    best_s = 0

    optional_theta = [float('-inf')]
    optional_theta += (pairwise_avg(dataset))

    for theta in optional_theta:
        for s in [-1, 1]:
            curr_ein = calc_ein(dataset, results, s, theta)

            if (curr_ein < min_ein or (curr_ein == min_ein and theta*s < best_theta*best_s)):
                min_ein = curr_ein
                best_theta = theta
                best_s = s

    return (best_s, best_theta, min_ein)


ITERS = 10000
N = 128
TAU = 0

if __name__ == "__main__":
    eout_ein = []

    from tqdm import tqdm
    for i in tqdm(range(ITERS)):
        x,y = generate_sorted_dataset(N, TAU)
        s, theta, min_ein = train_decision_stump_model(x,y)
        eout_ein.append(calc_eout(theta, TAU) - min_ein)

    print(statistics.mean(eout_ein))
