# 13
import numpy as np

def get_ein_g_s(x, y):
    min_ein = float("inf")
    best_g = float("inf")
    best_s = 0


    for i in range(len(x) - 1):
        g = (x[i] + x[i+1]) / 2
        for s in [-1,1]:
            ein = 0
            for j in range(len(x)):
                y_pred = s*np.sign(x[j] - g)
                if y_pred != y[j]:
                    ein += 1
            if ein < min_ein or (ein == min_ein and s*g < best_s*best_g):
                min_ein = ein
                best_g = g
                best_s = s
    
    if all(val >= 0 for val in x) or all(val < 0 for val in x):
        min_ein = 0


    return min_ein, best_g, best_s


total_ediff = 0
n_experiments = 10000

for i in range(n_experiments):
    x_vals = []
    n_pts = 2

    for i in range(n_pts):
        x = np.random.uniform(-0.5, 0.5)
        x_vals.append(x)


    x_vals.sort()
    y_vals = [np.sign(x) for x in x_vals]

    ein, theta, s = get_ein_g_s(x_vals, y_vals)
    tau = 0
    eout = min(abs(theta),0.5)*(1-tau) + tau


    total_ediff += eout - ein

print(total_ediff / n_experiments)